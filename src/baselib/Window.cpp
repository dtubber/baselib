#include<baselib/Window.hpp>

using namespace baselib;

Window::Window(unsigned int width, unsigned int height, char* title)
{
    this->m_WindowHandle = glfwCreateWindow(width,height,title,NULL,NULL);
    this->m_Width = width;
    this->m_Height = height;
    this->m_Title = title;
    this->setClearColor(0.0f,0.0f,0.0f);
    glfwSwapInterval(1);
}
Window::~Window()
{
    glfwDestroyWindow(this->m_WindowHandle);
}

void Window::swapBuffers()
{
    glfwSwapBuffers(this->m_WindowHandle);
}

void Window::pollEvents()
{
    glfwPollEvents();
}

void Window::makeContextCurrent()
{
    glfwMakeContextCurrent(this->m_WindowHandle);
}

void Window::setClearColor(float R, float G, float B)
{
    glClearColor(R,G,B,0.0f);
}

void Window::clear()
{
    glClear(GL_COLOR_BUFFER_BIT);
}

int Window::shouldClose()
{
    return glfwWindowShouldClose(this->m_WindowHandle);
}

GLFWwindow* Window::getHandle()
{
    return this->m_WindowHandle;
}

