Prerequisites (Windows) 
* Follow these Instructions step-by-step. 
* Download and install the latest version of the Code::Blocks IDE (the one without bundled compiler) for Windows.
* Download and install the latest version of the TDM-GCC compiler, the TDM64 version.
* Use default locations for both.
* Download or clone the master branch of this repository.
* Extract the contained folder to a location of your choice.
* Extract the "lib_win64.7z" archive to the project folder
* Open the .cbp Project file with Code::Blocks
* Click on Build&Run.
* Enjoy the awesomeness!