#ifndef STATICMODEL_HPP
#define STATICMODEL_HPP

namespace baselib
{
    /**
    * \class StaticModel
    * \brief Represents a static 3D model.
    * \details Static 3D model, not animated. Best used for, well, static things,
    * like terrain, rocks etc...
    */
    class StaticModel
    {
    public:
    private:
    };
}

#endif // STATICMODEL_HPP
