#ifndef SHADER_HPP
#define SHADER_HPP

#include<GL/glew.h>

namespace baselib
{
    /**
    * \brief Represents an OpenGL Shader Program Object.
    */
    class Shader
    {
    public:
        Shader(char* filenameVS, char* filenameFS);
        ~Shader();

        unsigned int getHandle();
    private:
        unsigned int m_Handle;
    };
}

#endif // SHADER_HPP
