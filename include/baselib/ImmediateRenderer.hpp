#ifndef IMMEDIATERENDERER_HPP
#define IMMEDIATERENDERER_HPP

namespace baselib
{
    /**
    * \brief Provides OpenGL Immediate Mode functionality.
    */
    class ImmediateRenderer
    {
    public:
    private:
    };
}

#endif // IMMEDIATERENDERER_HPP
