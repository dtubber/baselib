#ifndef WINDOW_HPP
#define WINDOW_HPP

#include<GL/glew.h>
#include<GLFW/glfw3.h>

namespace baselib
{
    /**
    * \brief Wrapper class for a GLFW3 window.
    */
    class Window
    {
    public:
        /**
        * \brief Constructor.
        * \param width Desired width of the window.
        * \param height Desired height of the window.
        * \param title Desired title of the window.
        */
        Window(unsigned int width, unsigned int height, char* title);
        /**
        * \brief Destructor.
        */
        ~Window();
        /**
        * \brief Swaps current back- and frontbuffers.
        */
        void swapBuffers();
        /**
        * \brief Polls for any window-related events.
        */
        void pollEvents();
        /**
        * \brief Makes this context the current OpenGL context.
        */
        void makeContextCurrent();
        /**
        * \brief Sets the OpenGL clear color of this context.
        */
        void setClearColor(float R, float G, float B);
        /**
        * \brief Clears the current buffer.
        */
        void clear();
        /**
        * \brief Checks if the window is required to close.
        */
        int shouldClose();
        /**
        * \brief Returns the GLFW3 window handle.
        * \return The GLFW3 window handle.
        */
        GLFWwindow* getHandle();
    private:
        GLFWwindow* m_WindowHandle;
        int m_Width, m_Height;
        char* m_Title;
    };
}

#endif // WINDOW_HPP
